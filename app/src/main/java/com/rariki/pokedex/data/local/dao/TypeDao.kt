package com.rariki.pokedex.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.rariki.pokedex.data.local.entity.type.TypeLocal

@Dao
interface TypeDao {
    @Query("SELECT * FROM type")
    suspend fun getTypes():List<TypeLocal>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(vararg type:TypeLocal)

    @Query("DELETE FROM type")
    suspend fun deleteAll()
}