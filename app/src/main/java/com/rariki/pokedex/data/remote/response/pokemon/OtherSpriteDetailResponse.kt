package com.rariki.pokedex.data.remote.response.pokemon

import com.squareup.moshi.Json

data class OtherSpriteDetailResponse(
    @field:Json(name = "front_default") val frontDefault: String?,
    @field:Json(name = "front_female") val frontFemale: String?,
    @field:Json(name = "front_shiny") val frontShiny: String?,
    @field:Json(name = "front_shiny_female") val frontShinyFemale: String?,
)