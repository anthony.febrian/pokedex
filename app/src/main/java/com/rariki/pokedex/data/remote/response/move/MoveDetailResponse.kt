package com.rariki.pokedex.data.remote.response.move

import com.rariki.pokedex.data.remote.response.damageclass.DamageClassResponse
import com.rariki.pokedex.data.remote.response.type.ListTypeResponse
import com.squareup.moshi.Json

data class MoveDetailResponse(
    @field:Json(name = "id") val id: Int?,
    @field:Json(name = "name") val name: String?,
    @field:Json(name = "power") val power: Int?,
    @field:Json(name = "accuracy") val accuracy: Int?,
    @field:Json(name = "pp") val pp: Int?,
    @field:Json(name = "type") val type: ListTypeResponse?,
    @field:Json(name = "damage_class") val damageClass: DamageClassResponse?,
) {
}