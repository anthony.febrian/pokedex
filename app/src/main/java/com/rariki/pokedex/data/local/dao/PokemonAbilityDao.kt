package com.rariki.pokedex.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.rariki.pokedex.data.local.entity.pokemon.PokemonAbilityCrossRef

@Dao
interface PokemonAbilityDao {
    @Query("SELECT * FROM pokemon_ability_cross_ref")
    suspend fun getAll(): List<PokemonAbilityCrossRef>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(vararg pokemonMove: PokemonAbilityCrossRef)

    @Query("DELETE FROM pokemon_ability_cross_ref")
    suspend fun deleteAll()
}