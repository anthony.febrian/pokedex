package com.rariki.pokedex.data.remote.response.type

import com.rariki.pokedex.Constant
import com.rariki.pokedex.mapper.toLocal
import com.squareup.moshi.Json

data class ListTypeResponse(
    @field:Json(name = "name") val name: String?,
    @field:Json(name = "url") val url: String?,
) {
    fun getId():Int {
        return this.url?.removePrefix(Constant.TYPE_URL)
            ?.replace("/","")?.toInt() ?: -1
    }
}