package com.rariki.pokedex.data.local.entity.pokemon

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "pokemon_type_cross_ref", primaryKeys = ["pokemon_id", "type_id"])
data class PokemonTypeCrossRef(
    @ColumnInfo(name = "pokemon_id") val pokemonId: Int,
    @ColumnInfo(name = "type_id", index = true) val typeId: Int,
)