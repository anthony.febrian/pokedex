package com.rariki.pokedex.data.remote.response.ability

import com.squareup.moshi.Json

data class AbilityDetailResponse(
    @field:Json(name = "id") val id: Int?,
    @field:Json(name = "name") val name: String?,
    @field:Json(name = "effect_entries") val effectEntries: List<EffectEntriesResponse>?,
)