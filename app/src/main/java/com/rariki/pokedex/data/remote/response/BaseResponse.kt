package com.rariki.pokedex.data.remote.response

import com.squareup.moshi.Json

data class BaseResponse<T>(
    @field:Json(name = "count") val count: Int?,
    @field:Json(name = "next") val next: String?,
    @field:Json(name = "previous") val previous: String?,
    @field:Json(name = "results") val results: T,
)