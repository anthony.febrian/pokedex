package com.rariki.pokedex.data.local.entity.pokemon

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "pokemon_move_cross_ref", primaryKeys = ["pokemon_id", "move_id"])
data class PokemonMoveCrossRef(
    @ColumnInfo(name = "pokemon_id") val pokemonId: Int,
    @ColumnInfo(name = "move_id", index = true) val moveId: Int,
)