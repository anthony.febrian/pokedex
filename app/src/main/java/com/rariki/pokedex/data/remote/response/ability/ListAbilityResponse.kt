package com.rariki.pokedex.data.remote.response.ability

import com.squareup.moshi.Json

data class ListAbilityResponse(
    @field:Json(name = "name") val name: String?,
    @field:Json(name = "url") val url: String?,
)