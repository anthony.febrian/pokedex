package com.rariki.pokedex.data.remote.response.move

import com.squareup.moshi.Json

data class ListMoveResponse(
    @field:Json(name = "name") val name: String?,
    @field:Json(name = "url") val url: String?,
)