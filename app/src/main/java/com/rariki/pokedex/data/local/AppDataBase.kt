package com.rariki.pokedex.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.rariki.pokedex.data.local.dao.*
import com.rariki.pokedex.data.local.entity.ability.AbilityLocal
import com.rariki.pokedex.data.local.entity.move.MoveLocal
import com.rariki.pokedex.data.local.entity.pokemon.PokemonAbilityCrossRef
import com.rariki.pokedex.data.local.entity.pokemon.PokemonLocal
import com.rariki.pokedex.data.local.entity.pokemon.PokemonMoveCrossRef
import com.rariki.pokedex.data.local.entity.pokemon.PokemonTypeCrossRef
import com.rariki.pokedex.data.local.entity.type.TypeLocal

@Database(
    entities = [
        PokemonLocal::class,
        MoveLocal::class,
        PokemonMoveCrossRef::class,
        TypeLocal::class,
        PokemonTypeCrossRef::class,
        AbilityLocal::class,
        PokemonAbilityCrossRef::class,
    ],
    version = 1,
    exportSchema = false,
)
abstract class AppDataBase : RoomDatabase() {
    abstract fun pokemonDao(): PokemonDao
    abstract fun pokemonMoveDao(): PokemonMoveDao
    abstract fun moveDao(): MoveDao
    abstract fun pokemonTypeDao(): PokemonTypeDao
    abstract fun typeDao(): TypeDao
    abstract fun abilityDao(): AbilityDao
    abstract fun pokemonAbilityDao(): PokemonAbilityDao
}