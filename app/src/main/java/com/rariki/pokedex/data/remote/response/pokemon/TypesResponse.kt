package com.rariki.pokedex.data.remote.response.pokemon

import com.squareup.moshi.Json

data class TypesResponse(
    @field:Json(name = "slot") val slot: Int?,
    @field:Json(name = "type") val type: TypeResponse?,
) {
    data class TypeResponse(
        @field:Json(name = "name") val name: String?,
        @field:Json(name = "url") val url: String?,
    )
}