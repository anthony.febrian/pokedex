package com.rariki.pokedex.data.local.entity.type

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "type")
data class TypeLocal(
    @PrimaryKey @ColumnInfo(name = "type_id") val id:Int,
    @ColumnInfo(name = "name") val name:String,
)