package com.rariki.pokedex.data.local.dao

import androidx.room.*
import com.rariki.pokedex.data.local.entity.move.MoveLocal

@Dao
interface MoveDao {
    @Query("SELECT * FROM move")
    suspend fun getMoves(): List<MoveLocal>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(vararg move: MoveLocal)

    @Query("DELETE FROM move")
    suspend fun deleteAll()
}