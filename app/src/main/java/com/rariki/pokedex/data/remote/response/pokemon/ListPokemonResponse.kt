package com.rariki.pokedex.data.remote.response.pokemon

import com.squareup.moshi.Json

data class ListPokemonResponse(
    @field:Json(name = "name") val name:String?,
    @field:Json(name = "url") val url:String?,
)