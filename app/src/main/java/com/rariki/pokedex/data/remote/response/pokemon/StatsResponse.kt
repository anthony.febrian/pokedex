package com.rariki.pokedex.data.remote.response.pokemon

import com.squareup.moshi.Json

data class StatsResponse(
    @field:Json(name = "base_stat") val baseStat: Int?,
    @field:Json(name = "effort") val effort: Int?,
    @field:Json(name = "stat") val stat: StatResponse?,

    ) {
    data class StatResponse(
        @field:Json(name = "name") val stat: String?,
        @field:Json(name = "url") val url: String?,
    )
}