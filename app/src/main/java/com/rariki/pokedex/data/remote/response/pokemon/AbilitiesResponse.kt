package com.rariki.pokedex.data.remote.response.pokemon

import com.squareup.moshi.Json

data class AbilitiesResponse(
    @field:Json(name = "ability") val ability: AbilityResponse?,
    @field:Json(name = "is_hidden") val isHidden: Boolean?,
    @field:Json(name = "slot") val slot: Int?,
) {

    data class AbilityResponse(
        @field:Json(name = "name") val name: String?,
        @field:Json(name = "url") val url: String?,
    )
}