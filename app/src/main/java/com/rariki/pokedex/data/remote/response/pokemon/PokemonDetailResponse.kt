package com.rariki.pokedex.data.remote.response.pokemon

import com.squareup.moshi.Json

data class PokemonDetailResponse(
    @field:Json(name = "id") val id: Int?,
    @field:Json(name = "name") val name: String?,
    @field:Json(name = "abilities") val abilities: List<AbilitiesResponse>?,
    @field:Json(name = "moves") val moves: List<MoveResponse>?,
    @field:Json(name = "sprites") val sprites: SpriteResponse?,
    @field:Json(name = "types") val types: List<TypesResponse>?,
    @field:Json(name = "stats") val stats: List<StatsResponse>?,
    @field:Json(name = "weight") val weight: Int?,
    @field:Json(name = "height") val height: Int?,
    @field:Json(name = "base_experience") val baseExperience: Int?,
)