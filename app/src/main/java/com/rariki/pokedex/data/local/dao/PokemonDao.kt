package com.rariki.pokedex.data.local.dao

import androidx.room.*
import com.rariki.pokedex.data.local.entity.pokemon.PokemonFullDetailLocal
import com.rariki.pokedex.data.local.entity.pokemon.PokemonLocal

@Dao
interface PokemonDao {
    @Query("SELECT * FROM pokemon")
    suspend fun getListPokemon(): List<PokemonLocal>

    @Transaction
    @Query("SELECT * FROM pokemon WHERE pokemon_id = :id")
    suspend fun getPokemon(id: Int): PokemonFullDetailLocal?

    @Query("SELECT * FROM pokemon WHERE name LIKE '%' || :name || '%'")
    suspend fun searchPokemon(name: String): List<PokemonLocal>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPokemon(vararg pokemon: PokemonLocal)

    @Query("DELETE FROM pokemon")
    suspend fun deleteAll()

}