package com.rariki.pokedex.data.remote.response.pokemon

import com.squareup.moshi.Json

data class OtherSpriteResponse(
    @field:Json(name = "dream_world") val dreamWorld: OtherSpriteDetailResponse?,
    @field:Json(name = "home") val home: OtherSpriteDetailResponse?,
    @field:Json(name = "official-artwork") val officialArtWork: OtherSpriteDetailResponse?,
)