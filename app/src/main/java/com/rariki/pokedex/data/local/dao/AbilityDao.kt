package com.rariki.pokedex.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.rariki.pokedex.data.local.entity.ability.AbilityLocal

@Dao
interface AbilityDao {
    @Query("SELECT * FROM ability")
    suspend fun getAll(): List<AbilityLocal>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(vararg move: AbilityLocal)

    @Query("DELETE FROM ability")
    suspend fun deleteAll()
}