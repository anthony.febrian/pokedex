package com.rariki.pokedex.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.rariki.pokedex.data.local.entity.pokemon.PokemonMoveCrossRef

@Dao
interface PokemonMoveDao {
    @Query("SELECT * FROM pokemon_move_cross_ref")
    suspend fun getAll(): List<PokemonMoveCrossRef>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(vararg pokemonMove: PokemonMoveCrossRef)

    @Query("DELETE FROM pokemon_move_cross_ref")
    suspend fun deleteAll()
}