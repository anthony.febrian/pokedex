package com.rariki.pokedex.data.local.entity.pokemon

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "pokemon_ability_cross_ref", primaryKeys = ["pokemon_id", "ability_id"])
data class PokemonAbilityCrossRef(
    @ColumnInfo(name = "pokemon_id") val pokemonId: Int,
    @ColumnInfo(name = "ability_id", index = true) val abilityId: Int,
)
