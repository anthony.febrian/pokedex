package com.rariki.pokedex.data.local.entity.ability

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "ability")
data class AbilityLocal(
    @PrimaryKey @ColumnInfo(name = "ability_id") val id: Int,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "description") val description: String,
)