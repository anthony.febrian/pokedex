package com.rariki.pokedex.data.local.entity.move

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "move")
data class MoveLocal(
    @PrimaryKey @ColumnInfo(name = "move_id") val id: Int,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "power") val power: Int,
    @ColumnInfo(name = "accuracy") val accuracy: Int,
    @ColumnInfo(name = "pp") val pp: Int,
    @ColumnInfo(name = "type_id") val typeId: Int,
    @ColumnInfo(name = "damage_class_id") val damageClassId: Int?,
)