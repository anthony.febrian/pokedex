package com.rariki.pokedex.data.remote.response.damageclass

import com.squareup.moshi.Json

data class DamageClassResponse(
    @field:Json(name = "name") val name: String?,
    @field:Json(name = "url") val url: String?,
)