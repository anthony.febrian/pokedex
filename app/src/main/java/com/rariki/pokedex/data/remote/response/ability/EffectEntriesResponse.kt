package com.rariki.pokedex.data.remote.response.ability

import com.squareup.moshi.Json

data class EffectEntriesResponse(
    @field:Json(name = "effect") val effect: String?,
    @field:Json(name = "short_effect") val shortEffect: String?,
    @field:Json(name = "language") val language: Language?
) {
    data class Language(
        @field:Json(name = "name") val name: String?,
        @field:Json(name = "url") val url: String?,
    )
}