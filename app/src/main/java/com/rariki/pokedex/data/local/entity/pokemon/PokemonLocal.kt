package com.rariki.pokedex.data.local.entity.pokemon

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "pokemon")
data class PokemonLocal(
    @PrimaryKey @ColumnInfo(name = "pokemon_id") val id: Int,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "weight") val weight: Int,
    @ColumnInfo(name = "height") val height: Int,
    @ColumnInfo(name = "base_exp") val baseExp: Int,

    //sprite
    @ColumnInfo(name = "sprite") val sprite:String,
    @ColumnInfo(name = "sprite_female") val spriteFemale:String,
    @ColumnInfo(name = "official_art_work") val officialArtWork:String,
)