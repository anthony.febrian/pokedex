package com.rariki.pokedex.data.repository

import com.orhanobut.hawk.Hawk
import com.rariki.pokedex.data.local.dao.*
import com.rariki.pokedex.data.remote.PokedexService
import com.rariki.pokedex.domain.entity.ListPokemon
import com.rariki.pokedex.domain.entity.ProgressDownload
import com.rariki.pokedex.domain.repository.PokedexRepository
import com.rariki.pokedex.mapper.toCrossRef
import com.rariki.pokedex.mapper.toListPokemonDomain
import com.rariki.pokedex.mapper.toLocal
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class PokedexRepositoryImpl(
    private val service: PokedexService,
    private val pokemonDao: PokemonDao,
    private val pokemonMoveDao: PokemonMoveDao,
    private val pokemonTypeDao: PokemonTypeDao,
    private val pokemonAbilityDao: PokemonAbilityDao,
    private val typeDao: TypeDao,
    private val abilityDao: AbilityDao,
    private val moveDao: MoveDao,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : PokedexRepository {
    override fun downloadPokemon(): Flow<ProgressDownload> {

        return flow {
            if (isPokemonDownloaded()) {
                emit(ProgressDownload.Done)
                return@flow
            }

            var progress = 0
            emit(
                ProgressDownload.InProgress(
                    progress = progress,
                    total = TOTAL_POKEMON
                )
            )

            val response = service.getPokemon()
            if (response.isSuccessful) {
                val result = response.body()!!.results
                val pokemonLocal = result
                    .map { it.toLocal() }
                    .toTypedArray()
                pokemonDao.insertPokemon(*pokemonLocal)

                //download detail
                pokemonLocal.forEach {
                    val responseDetail = service.getPokemonDetail(it.id)
                    if (responseDetail.isSuccessful) {

                        val pokemonDetail = responseDetail.body()!!
                        val pokemonId = pokemonDetail.id ?: -1

                        //insert detail
                        pokemonDao.insertPokemon(responseDetail.body()!!.toLocal())

                        //insert move
                        pokemonDetail.moves?.let {
                            val arrayMove = it
                                .map {
                                    it.toCrossRef(pokemonId)
                                }
                                .filter { it.pokemonId > 0 && it.moveId > 0 }
                                .toTypedArray()

                            pokemonMoveDao.insert(*arrayMove)
                        }

                        //insert ability
                        pokemonDetail.abilities?.let {
                            val arrayAbility = it
                                .map {
                                    it.toCrossRef(pokemonId)
                                }
                                .filter { it.pokemonId > 0 && it.abilityId > 0 }
                                .toTypedArray()

                            pokemonAbilityDao.insert(*arrayAbility)
                        }

                        //insert type
                        pokemonDetail.types?.let {
                            val arrayType = it
                                .map {
                                    it.toCrossRef(pokemonId)
                                }
                                .filter { it.pokemonId > 0 && it.typeId > 0 }
                                .toTypedArray()

                            pokemonTypeDao.insert(*arrayType)
                        }

                    }
                    progress++
                    emit(
                        ProgressDownload.InProgress(
                            progress = progress,
                            total = TOTAL_POKEMON
                        )
                    )
                }

                Hawk.put(KEY_POKEMON, true)

                emit(ProgressDownload.Done)
            } else {
                emit(ProgressDownload.Failed)
            }
        }.flowOn(dispatcher)
    }

    override fun downloadType(): Flow<ProgressDownload> {
        return flow {
            if (isTypeDownloaded()) {
                emit(ProgressDownload.Done)
                return@flow
            }

            emit(
                ProgressDownload.InProgress(
                    progress = 0,
                    total = TOTAL_TYPE
                )
            )
            val response = service.getType()
            if (response.isSuccessful) {
                val result = response.body()!!.results
                val typeLocal = result
                    .map { it.toLocal() }
                    .toTypedArray()
                typeDao.insert(*typeLocal)

                Hawk.put(KEY_TYPE, true)
                emit(ProgressDownload.Done)
            } else {
                emit(ProgressDownload.Failed)
            }

        }.flowOn(dispatcher)
    }

    override fun downloadAbility(): Flow<ProgressDownload> {
        return flow {
            if (isAbilityDownloaded()) {
                emit(ProgressDownload.Done)
                return@flow
            }

            var progress = 0
            emit(
                ProgressDownload.InProgress(
                    progress = progress,
                    total = TOTAL_ABILITY
                )
            )

            val response = service.getAbilities()
            if (response.isSuccessful) {
                val result = response.body()!!.results
                val abilitiesLocal = result
                    .map { it.toLocal() }
                    .toTypedArray()
                abilityDao.insert(*abilitiesLocal)

                //download detail
                abilitiesLocal.forEach {
                    val responseDetail = service.getAbility(it.id)
                    if (responseDetail.isSuccessful) {
                        val abilityDetails = responseDetail.body()!!.toLocal()

                        abilityDao.insert(abilityDetails)
                    }

                    progress++
                    emit(
                        ProgressDownload.InProgress(
                            progress = progress,
                            total = TOTAL_ABILITY
                        )
                    )
                }

                Hawk.put(KEY_ABILITY, true)
                emit(ProgressDownload.Done)
            } else {
                emit(ProgressDownload.Failed)
            }
        }
    }

    override fun downloadMove(): Flow<ProgressDownload> {
        return flow {
            if (isMoveDownloaded()) {
                emit(ProgressDownload.Done)
                return@flow
            }

            var progress = 0
            emit(
                ProgressDownload.InProgress(
                    progress = progress,
                    total = TOTAL_MOVE
                )
            )

            val response = service.getMoves()
            if (response.isSuccessful) {
                val result = response.body()!!.results
                val movesLocal = result
                    .map { it.toLocal() }
                    .toTypedArray()
                moveDao.insert(*movesLocal)

                //download detail
                movesLocal.forEach {
                    val responseDetail = service.getMove(it.id)
                    if (responseDetail.isSuccessful) {
                        val abilityDetails = responseDetail.body()!!.toLocal()

                        moveDao.insert(abilityDetails)
                    }

                    progress++
                    emit(
                        ProgressDownload.InProgress(
                            progress = progress,
                            total = TOTAL_MOVE
                        )
                    )
                }

                Hawk.put(KEY_MOVE, true)
                emit(ProgressDownload.Done)
            } else {
                emit(ProgressDownload.Failed)
            }
        }
    }

    override fun isPokemonDownloaded(): Boolean {
        return Hawk.get(KEY_POKEMON, false)
    }

    override fun isTypeDownloaded(): Boolean {
        return Hawk.get(KEY_TYPE, false)
    }

    override fun isAbilityDownloaded(): Boolean {
        return Hawk.get(KEY_ABILITY, false)
    }

    override fun isMoveDownloaded(): Boolean {
        return Hawk.get(KEY_MOVE, false)
    }

    override suspend fun getPokemon(): List<ListPokemon> {
        return pokemonDao.getListPokemon().map {
            it.toListPokemonDomain()
        }
    }

    companion object {
        private const val TOTAL_POKEMON = 905
        private const val TOTAL_TYPE = 18
        private const val TOTAL_MOVE = 844
        private const val TOTAL_ABILITY = 327

        private const val KEY_POKEMON = "PokedexRepository.Pokemon"
        private const val KEY_TYPE = "PokedexRepository.Type"
        private const val KEY_MOVE = "PokedexRepository.Move"
        private const val KEY_ABILITY = "PokedexRepository.Ability"
    }
}