package com.rariki.pokedex.data.remote.response.pokemon

import com.squareup.moshi.Json

data class MoveResponse(
    @field:Json(name = "move") val move: DetailResponse?
) {
    data class DetailResponse(
        @field:Json(name = "name") val name: String?,
        @field:Json(name = "url") val url: String?
    )
}