package com.rariki.pokedex.data.remote

import com.rariki.pokedex.data.remote.response.BaseResponse
import com.rariki.pokedex.data.remote.response.ability.AbilityDetailResponse
import com.rariki.pokedex.data.remote.response.ability.ListAbilityResponse
import com.rariki.pokedex.data.remote.response.damageclass.DamageClassResponse
import com.rariki.pokedex.data.remote.response.move.ListMoveResponse
import com.rariki.pokedex.data.remote.response.move.MoveDetailResponse
import com.rariki.pokedex.data.remote.response.pokemon.ListPokemonResponse
import com.rariki.pokedex.data.remote.response.pokemon.PokemonDetailResponse
import com.rariki.pokedex.data.remote.response.type.ListTypeResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface PokedexService {

    @GET("pokemon?limit=905")
    suspend fun getPokemon(): Response<BaseResponse<List<ListPokemonResponse>>>

    @GET("pokemon/{id}")
    suspend fun getPokemonDetail(@Path("id") id: Int): Response<PokemonDetailResponse>

    @GET("type")
    suspend fun getType(): Response<BaseResponse<List<ListTypeResponse>>>

    @GET("move-damage-class")
    suspend fun getDamageClass(): Response<BaseResponse<List<DamageClassResponse>>>

    @GET("move?limit=844")
    suspend fun getMoves(): Response<BaseResponse<List<ListMoveResponse>>>

    @GET("move/{id}")
    suspend fun getMove(@Path("id") id: Int): Response<MoveDetailResponse>

    @GET("ability?limit=327")
    suspend fun getAbilities(): Response<BaseResponse<List<ListAbilityResponse>>>

    @GET("ability/{id}")
    suspend fun getAbility(@Path("id") id: Int): Response<AbilityDetailResponse>
}