package com.rariki.pokedex.data.local.entity.pokemon

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import com.rariki.pokedex.data.local.entity.ability.AbilityLocal
import com.rariki.pokedex.data.local.entity.move.MoveLocal
import com.rariki.pokedex.data.local.entity.type.TypeLocal

data class PokemonFullDetailLocal(
    @Embedded val pokemon: PokemonLocal,
    @Relation(
        parentColumn = "pokemon_id",
        entityColumn = "move_id",
        associateBy = Junction(PokemonMoveCrossRef::class)
    )
    val moves: List<MoveLocal>,
    @Relation(
        parentColumn = "pokemon_id",
        entityColumn = "type_id",
        associateBy = Junction(PokemonTypeCrossRef::class)
    )
    val types: List<TypeLocal>,

    @Relation(
        parentColumn = "pokemon_id",
        entityColumn = "ability_id",
        associateBy = Junction(PokemonAbilityCrossRef::class)
    )
    val abilities: List<AbilityLocal>,
)