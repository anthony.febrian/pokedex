package com.rariki.pokedex.data.remote.response.pokemon

import com.squareup.moshi.Json

data class SpriteResponse(
    @field:Json(name = "back_default") val backDefault: String?,
    @field:Json(name = "back_female") val backFemale: String?,
    @field:Json(name = "back_shiny") val backShiny: String?,
    @field:Json(name = "back_shiny_female") val backShinyFemale: String?,
    @field:Json(name = "front_default") val frontDefault: String?,
    @field:Json(name = "front_female") val frontFemale: String?,
    @field:Json(name = "front_shiny") val frontShiny: String?,
    @field:Json(name = "front_shiny_female") val frontShinyFemale: String?,
    @field:Json(name = "other") val other: OtherSpriteResponse?,
)