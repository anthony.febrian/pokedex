package com.rariki.pokedex.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.rariki.pokedex.data.local.entity.pokemon.PokemonTypeCrossRef


@Dao
interface PokemonTypeDao {
    @Query("SELECT * FROM pokemon_type_cross_ref")
    suspend fun getAll(): List<PokemonTypeCrossRef>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(vararg pokemonMove: PokemonTypeCrossRef)

    @Query("DELETE FROM pokemon_type_cross_ref")
    suspend fun deleteAll()
}