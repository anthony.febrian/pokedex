package com.rariki.pokedex

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.rariki.pokedex.domain.entity.ProgressDownload
import com.rariki.pokedex.domain.repository.PokedexRepository
import com.rariki.pokedex.presentation.screen.DownloadScreen
import com.rariki.pokedex.presentation.screen.MainScreen
import com.rariki.pokedex.presentation.theme.PokedexTheme
import com.rariki.pokedex.route.Routes
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    @Inject lateinit var repository:PokedexRepository
    private val progress = MutableStateFlow<ProgressDownload?>(null)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            PokedexTheme {
                val navController = rememberNavController()
                NavHost(navController = navController, startDestination = Routes.download) {

                    composable(Routes.download) {
                        DownloadScreen {
                            navController.navigate(Routes.main) {
                                popUpTo(Routes.download) { inclusive = true }
                            }
                        }
                    }

                    composable(Routes.main) {
                        MainScreen {

                        }
                    }
                }
            }
        }

        repository.downloadPokemon()
            .onEach {
                progress.value = it
            }
            .launchIn(CoroutineScope(Dispatchers.IO))
    }
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    PokedexTheme {
        Greeting("Android")
    }
}