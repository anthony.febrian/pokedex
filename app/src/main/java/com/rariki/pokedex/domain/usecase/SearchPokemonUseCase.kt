package com.rariki.pokedex.domain.usecase

import com.rariki.pokedex.domain.entity.ListPokemon

interface SearchPokemonUseCase {
    operator fun invoke(keyword:String, pokemon:List<ListPokemon>):List<ListPokemon>
}