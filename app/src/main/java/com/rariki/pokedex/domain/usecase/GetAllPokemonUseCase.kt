package com.rariki.pokedex.domain.usecase

import com.rariki.pokedex.domain.entity.ListPokemon
import kotlinx.coroutines.flow.Flow

interface GetAllPokemonUseCase {
    operator fun invoke():Flow<List<ListPokemon>>
}