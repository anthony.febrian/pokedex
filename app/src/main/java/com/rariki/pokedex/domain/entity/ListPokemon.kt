package com.rariki.pokedex.domain.entity

data class ListPokemon(
    val id:Int,
    val name:String,
    val sprite:String
)