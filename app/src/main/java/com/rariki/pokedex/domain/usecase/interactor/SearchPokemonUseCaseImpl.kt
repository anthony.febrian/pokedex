package com.rariki.pokedex.domain.usecase.interactor

import com.rariki.pokedex.domain.entity.ListPokemon
import com.rariki.pokedex.domain.usecase.SearchPokemonUseCase

class SearchPokemonUseCaseImpl : SearchPokemonUseCase {

    override fun invoke(keyword: String, pokemon: List<ListPokemon>): List<ListPokemon> {
        return pokemon
            .filter {
                if (keyword.isEmpty()) {
                    return@filter true
                } else {
                    return@filter it.name.contains(keyword, ignoreCase = true)
                }
            }
    }
}