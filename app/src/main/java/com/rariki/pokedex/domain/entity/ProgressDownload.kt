package com.rariki.pokedex.domain.entity

sealed class ProgressDownload {
    object None : ProgressDownload()
    data class InProgress(
        val total: Int,
        val progress: Int,
    ) : ProgressDownload() {
        val progressPercentage: Float
            get() {
                return progress.toFloat() / total.toFloat()
            }
    }

    object Done : ProgressDownload()
    object Failed : ProgressDownload()
}