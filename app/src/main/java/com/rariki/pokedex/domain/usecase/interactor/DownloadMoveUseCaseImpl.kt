package com.rariki.pokedex.domain.usecase.interactor

import com.rariki.pokedex.domain.entity.ProgressDownload
import com.rariki.pokedex.domain.repository.PokedexRepository
import com.rariki.pokedex.domain.usecase.DownloadMoveUseCase
import kotlinx.coroutines.flow.Flow

class DownloadMoveUseCaseImpl(
    private val repository: PokedexRepository
) : DownloadMoveUseCase {
    override fun invoke(): Flow<ProgressDownload> {
        return repository.downloadMove()
    }
}