package com.rariki.pokedex.domain.repository

import com.rariki.pokedex.domain.entity.ListPokemon
import com.rariki.pokedex.domain.entity.Pokemon
import com.rariki.pokedex.domain.entity.ProgressDownload
import kotlinx.coroutines.flow.Flow

interface PokedexRepository {
    fun downloadPokemon(): Flow<ProgressDownload>
    fun downloadType(): Flow<ProgressDownload>
    fun downloadAbility(): Flow<ProgressDownload>
    fun downloadMove(): Flow<ProgressDownload>

    fun isPokemonDownloaded(): Boolean
    fun isTypeDownloaded(): Boolean
    fun isAbilityDownloaded(): Boolean
    fun isMoveDownloaded(): Boolean

    suspend fun getPokemon(): List<ListPokemon>
}