package com.rariki.pokedex.domain.usecase.interactor

import com.rariki.pokedex.domain.entity.ListPokemon
import com.rariki.pokedex.domain.repository.PokedexRepository
import com.rariki.pokedex.domain.usecase.GetAllPokemonUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class GetAllPokemonUseCaseImpl(private val repository: PokedexRepository): GetAllPokemonUseCase {
    override fun invoke(): Flow<List<ListPokemon>> {
        return flow {
            emit(repository.getPokemon())
        }
    }
}