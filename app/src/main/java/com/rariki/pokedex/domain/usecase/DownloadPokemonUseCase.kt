package com.rariki.pokedex.domain.usecase

import com.rariki.pokedex.domain.entity.ProgressDownload
import kotlinx.coroutines.flow.Flow

interface DownloadPokemonUseCase {
    operator fun invoke(): Flow<ProgressDownload>
}