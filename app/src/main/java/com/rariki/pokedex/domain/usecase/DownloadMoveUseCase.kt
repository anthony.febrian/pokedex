package com.rariki.pokedex.domain.usecase

import com.rariki.pokedex.domain.entity.ProgressDownload
import kotlinx.coroutines.flow.Flow

interface DownloadMoveUseCase {
    operator fun invoke(): Flow<ProgressDownload>
}