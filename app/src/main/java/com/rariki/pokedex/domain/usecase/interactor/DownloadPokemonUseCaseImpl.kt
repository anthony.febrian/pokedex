package com.rariki.pokedex.domain.usecase.interactor

import com.rariki.pokedex.domain.entity.ProgressDownload
import com.rariki.pokedex.domain.repository.PokedexRepository
import com.rariki.pokedex.domain.usecase.DownloadPokemonUseCase
import kotlinx.coroutines.flow.Flow

class DownloadPokemonUseCaseImpl(
    private val repository: PokedexRepository
) : DownloadPokemonUseCase {
    override fun invoke(): Flow<ProgressDownload> {
        return repository.downloadPokemon()
    }
}