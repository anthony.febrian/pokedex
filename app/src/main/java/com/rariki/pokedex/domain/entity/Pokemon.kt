package com.rariki.pokedex.domain.entity

data class Pokemon(
    val id:Int,
    val name:String,
    val url:String,
)