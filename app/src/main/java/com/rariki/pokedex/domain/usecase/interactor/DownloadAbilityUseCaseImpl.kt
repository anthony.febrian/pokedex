package com.rariki.pokedex.domain.usecase.interactor

import com.rariki.pokedex.domain.entity.ProgressDownload
import com.rariki.pokedex.domain.repository.PokedexRepository
import com.rariki.pokedex.domain.usecase.DownloadAbilityUseCase
import kotlinx.coroutines.flow.Flow

class DownloadAbilityUseCaseImpl(
    private val repository: PokedexRepository
):DownloadAbilityUseCase {
    override fun invoke(): Flow<ProgressDownload> {
        return repository.downloadAbility()
    }
}