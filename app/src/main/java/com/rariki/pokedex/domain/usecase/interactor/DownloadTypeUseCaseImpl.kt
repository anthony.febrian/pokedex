package com.rariki.pokedex.domain.usecase.interactor

import com.rariki.pokedex.domain.entity.ProgressDownload
import com.rariki.pokedex.domain.repository.PokedexRepository
import com.rariki.pokedex.domain.usecase.DownloadTypeUseCase
import kotlinx.coroutines.flow.Flow

class DownloadTypeUseCaseImpl(
    private val repository: PokedexRepository
) : DownloadTypeUseCase {
    override fun invoke(): Flow<ProgressDownload> {
        return repository.downloadType()
    }
}