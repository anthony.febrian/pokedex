package com.rariki.pokedex.di

import android.content.Context
import androidx.room.Room
import com.rariki.pokedex.data.local.AppDataBase
import com.rariki.pokedex.data.local.dao.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object LocalModule {

    @Provides
    fun provideDatabase(@ApplicationContext context: Context): AppDataBase {
        return Room.databaseBuilder(
            context,
            AppDataBase::class.java, "pokedex"
        ).build()
    }

    @Provides
    fun providePokemonDao(db: AppDataBase): PokemonDao {
        return db.pokemonDao()
    }

    @Provides
    fun provideAbilityDao(db: AppDataBase): AbilityDao {
        return db.abilityDao()
    }

    @Provides
    fun provideMoveDao(db: AppDataBase): MoveDao {
        return db.moveDao()
    }

    @Provides
    fun provideTypeDao(db: AppDataBase): TypeDao {
        return db.typeDao()
    }

    //cross ref

    @Provides
    fun providePokemonAbilityDao(db: AppDataBase): PokemonAbilityDao {
        return db.pokemonAbilityDao()
    }

    @Provides
    fun providePokemonMoveDao(db: AppDataBase): PokemonMoveDao {
        return db.pokemonMoveDao()
    }

    @Provides
    fun providePokemonTypeDao(db: AppDataBase): PokemonTypeDao {
        return db.pokemonTypeDao()
    }
}