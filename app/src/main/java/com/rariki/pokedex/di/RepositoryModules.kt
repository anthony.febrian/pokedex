package com.rariki.pokedex.di

import com.rariki.pokedex.data.local.dao.*
import com.rariki.pokedex.data.remote.PokedexService
import com.rariki.pokedex.data.repository.PokedexRepositoryImpl
import com.rariki.pokedex.domain.repository.PokedexRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModules {
    @Provides
    fun providePokedexRepository(
        pokedexService: PokedexService,
        pokemonDao: PokemonDao,
        pokemonMoveDao: PokemonMoveDao,
        pokemonAbilityDao: PokemonAbilityDao,
        pokemonTypeDao: PokemonTypeDao,
        typeDao: TypeDao,
        moveDao: MoveDao,
        abilityDao: AbilityDao,
    ): PokedexRepository {
        return PokedexRepositoryImpl(
            service = pokedexService,
            pokemonDao = pokemonDao,
            pokemonMoveDao = pokemonMoveDao,
            pokemonAbilityDao = pokemonAbilityDao,
            pokemonTypeDao = pokemonTypeDao,
            typeDao = typeDao,
            abilityDao = abilityDao,
            moveDao = moveDao,
        )
    }
}