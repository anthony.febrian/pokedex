package com.rariki.pokedex.di

import com.rariki.pokedex.domain.repository.PokedexRepository
import com.rariki.pokedex.domain.usecase.*
import com.rariki.pokedex.domain.usecase.interactor.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
object UseCaseModule {
    @Provides
    fun provideDownloadPokemonUseCase(repository: PokedexRepository): DownloadPokemonUseCase {
        return DownloadPokemonUseCaseImpl(repository)
    }

    @Provides
    fun provideDownloadAbilityUseCase(repository: PokedexRepository): DownloadAbilityUseCase {
        return DownloadAbilityUseCaseImpl(repository)
    }

    @Provides
    fun provideDownloadMoveUseCase(repository: PokedexRepository): DownloadMoveUseCase {
        return DownloadMoveUseCaseImpl(repository)
    }

    @Provides
    fun provideDownloadTypeUseCase(repository: PokedexRepository): DownloadTypeUseCase {
        return DownloadTypeUseCaseImpl(repository)
    }

    @Provides
    fun provideGetAllPokemonUseCase(repository: PokedexRepository): GetAllPokemonUseCase {
        return GetAllPokemonUseCaseImpl(repository)
    }

    @Provides
    fun provideSearchPokemonUseCase() :SearchPokemonUseCase {
        return SearchPokemonUseCaseImpl()
    }
}