package com.rariki.pokedex.mapper

import com.rariki.pokedex.Constant
import com.rariki.pokedex.data.local.entity.ability.AbilityLocal
import com.rariki.pokedex.data.local.entity.pokemon.PokemonTypeCrossRef
import com.rariki.pokedex.data.local.entity.type.TypeLocal
import com.rariki.pokedex.data.remote.response.pokemon.TypesResponse
import com.rariki.pokedex.data.remote.response.type.ListTypeResponse

//from pokemon package
fun TypesResponse.toLocal(): TypeLocal {
    val id = this.type?.url?.removePrefix(Constant.TYPE_URL)
        ?.replace("/","")?.toInt() ?: -1
    return TypeLocal(
        id = id,
        name = this.type?.name ?: "",
    )
}

fun TypesResponse.toCrossRef(pokemonId:Int): PokemonTypeCrossRef {
    return PokemonTypeCrossRef(
        pokemonId = pokemonId,
        typeId = this.toLocal().id
    )
}

//from type package
fun ListTypeResponse.toLocal(): TypeLocal {
    val id = this.getId()
    return TypeLocal(
        id = id,
        name = this.name ?: ""
    )
}