package com.rariki.pokedex.mapper

import com.rariki.pokedex.data.local.entity.pokemon.PokemonLocal
import com.rariki.pokedex.data.remote.response.pokemon.ListPokemonResponse
import com.rariki.pokedex.data.remote.response.pokemon.PokemonDetailResponse
import com.rariki.pokedex.domain.entity.ListPokemon

fun ListPokemonResponse.toLocal(): PokemonLocal {
    val id = url?.removePrefix("https://pokeapi.co/api/v2/pokemon/")
        ?.replace("/", "")?.toInt() ?: -1
    return PokemonLocal(
        id = id,
        name = name ?: "",
        weight = 0,
        height = 0,
        baseExp = 0,
        sprite = "",
        spriteFemale = "",
        officialArtWork = "",
    )
}

fun PokemonDetailResponse.toLocal(): PokemonLocal {
    return PokemonLocal(
        id = id ?: -1,
        name = name ?: "",
        weight = weight ?: 0,
        height = height ?: 0,
        baseExp = baseExperience ?: 0,
        sprite = sprites?.frontDefault ?: "",
        spriteFemale = sprites?.frontFemale ?: "",
        officialArtWork = sprites?.other?.officialArtWork?.frontDefault ?: "",
    )
}

fun PokemonLocal.toListPokemonDomain(): ListPokemon {
    return ListPokemon(
        id = this.id,
        name = this.name,
        sprite = this.sprite
    )
}