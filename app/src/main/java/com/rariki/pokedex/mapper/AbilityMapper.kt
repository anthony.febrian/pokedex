package com.rariki.pokedex.mapper

import com.rariki.pokedex.Constant
import com.rariki.pokedex.data.local.entity.ability.AbilityLocal
import com.rariki.pokedex.data.local.entity.pokemon.PokemonAbilityCrossRef
import com.rariki.pokedex.data.remote.response.ability.AbilityDetailResponse
import com.rariki.pokedex.data.remote.response.ability.ListAbilityResponse
import com.rariki.pokedex.data.remote.response.pokemon.AbilitiesResponse

fun AbilitiesResponse.toLocal(): AbilityLocal {
    val id = this.ability?.url?.removePrefix(Constant.ABILITY_URL)
        ?.replace("/", "")?.toInt() ?: -1
    return AbilityLocal(
        id = id,
        name = this.ability?.name ?: "",
        description = "",
    )
}

fun AbilitiesResponse.toCrossRef(pokemonId: Int): PokemonAbilityCrossRef {
    return PokemonAbilityCrossRef(
        pokemonId = pokemonId,
        abilityId = this.toLocal().id
    )
}

fun ListAbilityResponse.toLocal(): AbilityLocal {
    val id = this.url?.removePrefix(Constant.ABILITY_URL)
        ?.replace("/", "")?.toInt() ?: -1
    return AbilityLocal(
        id = id,
        name = this.name ?: "",
        description = "",
    )
}

fun AbilityDetailResponse.toLocal(): AbilityLocal {
    return AbilityLocal(
        id = this.id ?: -1,
        name = this.name ?: "",
        description = this.effectEntries?.firstOrNull { it.language?.name == "en" }?.effect ?: ""
    )
}