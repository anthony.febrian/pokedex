package com.rariki.pokedex.mapper

import com.rariki.pokedex.Constant
import com.rariki.pokedex.data.local.entity.move.MoveLocal
import com.rariki.pokedex.data.local.entity.pokemon.PokemonMoveCrossRef
import com.rariki.pokedex.data.remote.response.move.ListMoveResponse
import com.rariki.pokedex.data.remote.response.move.MoveDetailResponse
import com.rariki.pokedex.data.remote.response.pokemon.MoveResponse

fun MoveResponse.toLocal(): MoveLocal {
    val id = this.move?.url
        ?.removePrefix(Constant.MOVE_URL)
        ?.replace("/", "")
        ?.toInt() ?: -1
    return MoveLocal(
        id = id,
        name = this.move?.name ?: "",
        power = 0,
        accuracy = 0,
        pp = 0,
        typeId = 0,
        damageClassId = 0,
    )
}

fun MoveResponse.toCrossRef(pokemonId: Int): PokemonMoveCrossRef {
    return PokemonMoveCrossRef(
        pokemonId = pokemonId,
        moveId = this.toLocal().id
    )
}

fun ListMoveResponse.toLocal(): MoveLocal {
    val id = this.url
        ?.removePrefix(Constant.MOVE_URL)
        ?.replace("/", "")
        ?.toInt() ?: -1

    return MoveLocal(
        id = id,
        name = this.name ?: "",
        power = 0,
        accuracy = 0,
        pp = 0,
        typeId = 0,
        damageClassId = 0,
    )
}

fun MoveDetailResponse.toLocal():MoveLocal {

    return MoveLocal(
        id = this.id ?: -1,
        name = this.name ?: "",
        power = this.power ?: 0,
        accuracy = this.accuracy ?: 0,
        pp = this.pp ?: 0,
        typeId = this.type?.getId() ?: -1,
        damageClassId = 0,
    )
}
