package com.rariki.pokedex

object Constant {
    const val TYPE_URL = "https://pokeapi.co/api/v2/type/"
    const val ABILITY_URL = "https://pokeapi.co/api/v2/ability/"
    const val MOVE_URL = "https://pokeapi.co/api/v2/move/"
}