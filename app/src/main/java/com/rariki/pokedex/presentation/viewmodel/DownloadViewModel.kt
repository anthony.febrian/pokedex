package com.rariki.pokedex.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rariki.pokedex.domain.entity.ProgressDownload
import com.rariki.pokedex.domain.usecase.DownloadAbilityUseCase
import com.rariki.pokedex.domain.usecase.DownloadMoveUseCase
import com.rariki.pokedex.domain.usecase.DownloadPokemonUseCase
import com.rariki.pokedex.domain.usecase.DownloadTypeUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class DownloadViewModel @Inject constructor(
    private val downloadPokemonUseCase: DownloadPokemonUseCase,
    private val downloadAbilityUseCase: DownloadAbilityUseCase,
    private val downloadMoveUseCase: DownloadMoveUseCase,
    private val downloadTypeUseCase: DownloadTypeUseCase,
) : ViewModel() {

    private val _downloadPokemonProgress = MutableStateFlow<ProgressDownload>(ProgressDownload.None)
    val downloadPokemonProgress = _downloadPokemonProgress.asStateFlow()

    private val _downloadAbilityProgress = MutableStateFlow<ProgressDownload>(ProgressDownload.None)
    val downloadAbilityProgress = _downloadAbilityProgress.asStateFlow()

    private val _downloadMoveProgress = MutableStateFlow<ProgressDownload>(ProgressDownload.None)
    val downloadMoveProgress = _downloadMoveProgress.asStateFlow()

    private val _downloadTypeProgress = MutableStateFlow<ProgressDownload>(ProgressDownload.None)
    val downloadTypeProgress = _downloadTypeProgress.asStateFlow()

    val done = combine(
        downloadPokemonProgress,
        downloadAbilityProgress,
        downloadMoveProgress,
        downloadTypeProgress
    ) { pokemon, ability, move, type ->
        pokemon is ProgressDownload.Done
                && ability is ProgressDownload.Done
                && move is ProgressDownload.Done
                && type is ProgressDownload.Done
    }


    init {
        downloadPokemon()
        downloadAbility()
        downloadMove()
        downloadType()
    }

    fun downloadType() {
        downloadTypeUseCase()
            .onEach { _downloadTypeProgress.value = it }
            .launchIn(viewModelScope)
    }

    fun downloadMove() {
        downloadMoveUseCase()
            .onEach { _downloadMoveProgress.value = it }
            .launchIn(viewModelScope)
    }

    fun downloadPokemon() {
        downloadPokemonUseCase()
            .onEach { _downloadPokemonProgress.value = it }
            .launchIn(viewModelScope)
    }

    fun downloadAbility() {
        downloadAbilityUseCase()
            .onEach { _downloadAbilityProgress.value = it }
            .launchIn(viewModelScope)
    }
}