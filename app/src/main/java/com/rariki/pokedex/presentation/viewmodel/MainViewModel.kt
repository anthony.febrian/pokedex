package com.rariki.pokedex.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rariki.pokedex.domain.entity.ListPokemon
import com.rariki.pokedex.domain.usecase.GetAllPokemonUseCase
import com.rariki.pokedex.domain.usecase.SearchPokemonUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val getAllPokemonUseCase: GetAllPokemonUseCase,
    private val searchPokemonUseCase: SearchPokemonUseCase,
): ViewModel() {

    private val _searchKeyWord = MutableStateFlow("")
    val searchKeyWord = _searchKeyWord.asStateFlow()

    private val _pokemon = MutableStateFlow<List<ListPokemon>>(listOf())
    val pokemon:StateFlow<List<ListPokemon>> = combine(searchKeyWord, _pokemon) { keyword, pokemon ->
        searchPokemonUseCase(keyword, pokemon)
    }.stateIn(
        scope = viewModelScope,
        started = SharingStarted.Eagerly,
        initialValue = listOf()
    )

    init {
        getPokemon()
    }

    private fun getPokemon() {
        getAllPokemonUseCase()
            .onEach { _pokemon.value = it }
            .launchIn(viewModelScope)
    }

    fun onKeyWordChange(keyword:String) {
        _searchKeyWord.value = keyword
    }
}