package com.rariki.pokedex.presentation.item

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.SubcomposeAsyncImage
import coil.request.ImageRequest
import com.rariki.pokedex.domain.entity.ListPokemon

@Composable
fun PokemonItem(
    pokemon: ListPokemon,
    onClick: (ListPokemon) -> Unit
) {

    Column(
        modifier = Modifier.padding(16.dp)
    ) {
        Card(modifier = Modifier.clickable { onClick(pokemon) }) {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.padding(16.dp)
            ) {
                SubcomposeAsyncImage(
                    model = ImageRequest.Builder(LocalContext.current)
                        .data(pokemon.sprite)
                        .crossfade(true)
                        .build(),
                    loading = {
                        CircularProgressIndicator()
                    },
                    contentDescription = null,
                    modifier = Modifier
                        .size(128.dp)
                )

                Text(text = pokemon.name)
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun Preview() {
    val pokemon = ListPokemon(
        id = 1,
        name = "bulbasaur",
        sprite = ""
    )

    Column(
        modifier = Modifier
            .padding(16.dp)
            .fillMaxSize()
    ) {

        PokemonItem(pokemon = pokemon, onClick = {})
    }
}