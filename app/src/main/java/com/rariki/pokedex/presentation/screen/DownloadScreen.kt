package com.rariki.pokedex.presentation.screen

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.rariki.pokedex.domain.entity.ProgressDownload
import com.rariki.pokedex.presentation.viewmodel.DownloadViewModel

@Composable
fun DownloadScreen(
    vm: DownloadViewModel = hiltViewModel(),
    goToMain: () -> Unit,
) {
    val done by vm.done.collectAsState(initial = false)

    val downloadProgressPokemon by vm.downloadPokemonProgress.collectAsState()
    val downloadProgressAbility by vm.downloadAbilityProgress.collectAsState()
    val downloadProgressMove by vm.downloadMoveProgress.collectAsState()
    val downloadProgressType by vm.downloadTypeProgress.collectAsState()

    Content(
        downloadProgressPokemon = downloadProgressPokemon,
        downloadProgressAbility = downloadProgressAbility,
        downloadProgressType = downloadProgressType,
        downloadProgressMove = downloadProgressMove,
        onRetryPokemon = vm::downloadPokemon,
        onRetryAbility = vm::downloadAbility,
        onRetryType = vm::downloadType,
        onRetryMove = vm::downloadMove,
    )

    LaunchedEffect(done) {
        if (done) {
            goToMain()
        }
    }
}

@Composable
private fun Content(
    downloadProgressPokemon: ProgressDownload,
    downloadProgressAbility: ProgressDownload,
    downloadProgressMove: ProgressDownload,
    downloadProgressType: ProgressDownload,
    onRetryPokemon: () -> Unit,
    onRetryAbility: () -> Unit,
    onRetryMove: () -> Unit,
    onRetryType: () -> Unit,
) {
    Column(
        modifier = Modifier
            .padding(16.dp)
            .fillMaxSize()
    ) {
        Text(text = "Downloading Data")

        ProgressDownloadView(
            title = "Pokemon",
            progress = downloadProgressPokemon,
            onRetry = onRetryPokemon,
        )
        Spacer(modifier = Modifier.height(16.dp))

        ProgressDownloadView(
            title = "Ability",
            progress = downloadProgressAbility,
            onRetry = onRetryAbility,
        )
        Spacer(modifier = Modifier.height(16.dp))

        ProgressDownloadView(
            title = "Move",
            progress = downloadProgressMove,
            onRetry = onRetryMove,
        )
        Spacer(modifier = Modifier.height(16.dp))

        ProgressDownloadView(
            title = "Type",
            progress = downloadProgressType,
            onRetry = onRetryType
        )
    }
}

@Composable
private fun ProgressDownloadView(
    title: String,
    progress: ProgressDownload,
    onRetry: () -> Unit,
) {

    Column {
        Text(text = title)

        Spacer(modifier = Modifier.height(16.dp))
        when (progress) {
            is ProgressDownload.Failed -> Text(
                text = "Failed",
                modifier = Modifier.clickable(onClick = onRetry)
            )
            is ProgressDownload.InProgress -> {
                LinearProgressIndicator(
                    progress = progress.progressPercentage,
                    modifier = Modifier.fillMaxWidth()
                )
                Text(text = "${progress.progress} / ${progress.total}")
            }
            is ProgressDownload.Done -> Text(text = "Done")
            else -> {}
        }

    }
}

@Preview(showBackground = true)
@Composable
private fun Preview() {
    val downloadProgressPokemon = ProgressDownload.InProgress(
        total = 905,
        progress = 25,
    )

    Content(
        downloadProgressPokemon = downloadProgressPokemon,
        downloadProgressAbility = downloadProgressPokemon,
        downloadProgressType = downloadProgressPokemon,
        downloadProgressMove = downloadProgressPokemon,
        onRetryPokemon = {},
        onRetryAbility = {},
        onRetryMove = {},
        onRetryType = {}
    )
}