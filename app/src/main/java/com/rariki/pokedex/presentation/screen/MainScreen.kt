package com.rariki.pokedex.presentation.screen

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.foundation.lazy.items
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.rariki.pokedex.domain.entity.ListPokemon
import com.rariki.pokedex.presentation.item.PokemonItem
import com.rariki.pokedex.presentation.viewmodel.MainViewModel

@Composable
fun MainScreen(
    vm: MainViewModel = hiltViewModel(),
    onClick: (ListPokemon) -> Unit,
) {

    val keyword by vm.searchKeyWord.collectAsState()
    val pokemon by vm.pokemon.collectAsState()

    Content(
        keyword = keyword,
        onKeywordChange = vm::onKeyWordChange,
        pokemon = pokemon,
        onClick = onClick
    )
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun Content(
    keyword: String,
    onKeywordChange: (String) -> Unit,
    pokemon: List<ListPokemon>,
    onClick: (ListPokemon) -> Unit,
) {

    Column(modifier = Modifier.fillMaxSize()) {

        OutlinedTextField(
            value = keyword,
            onValueChange = onKeywordChange,
            placeholder = {
                Text(text = "Search pokemon ...")
            },
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth()
        )

        LazyVerticalGrid(
            cells = GridCells.Fixed(count = 2)
        ) {
            items(pokemon) { pokemon ->
                PokemonItem(pokemon = pokemon, onClick = onClick)
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun Preview() {
    val keyword = ""
    val pokemon = ListPokemon(
        id = 1,
        name = "bulbasaur",
        sprite = ""
    )
    Content(
        keyword = keyword,
        onKeywordChange = {},
        pokemon = listOf(pokemon, pokemon),
        onClick = {})
}