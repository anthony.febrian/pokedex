@file:OptIn(ExperimentalCoroutinesApi::class)

package com.rariki.pokedex.data.remote

import kotlinx.coroutines.ExperimentalCoroutinesApi
import com.google.common.truth.Truth
import com.rariki.pokedex.utils.extention.setBodyFromFile
import kotlinx.coroutines.test.runTest
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.buffer
import okio.source
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.nio.charset.StandardCharsets
import java.util.concurrent.TimeUnit

class PokedexServiceTest {
    private val mockWebServer = MockWebServer()
    private val client = OkHttpClient.Builder()
        .connectTimeout(1, TimeUnit.SECONDS)
        .readTimeout(1, TimeUnit.SECONDS)
        .writeTimeout(1, TimeUnit.SECONDS)
        .build()

    private val service = Retrofit.Builder()
        .baseUrl(mockWebServer.url("/"))
        .client(client)
        .addConverterFactory(MoshiConverterFactory.create())
        .build()
        .create(PokedexService::class.java)


    @Test
    fun `Get List Pokemon`() = runTest {
        mockWebServer.enqueue(
            MockResponse().apply {
                addHeader("Content-Type", "application/json; charset=utf-8")
                addHeader("Cache-Control", "no-cache")
                setResponseCode(200)
                setBodyFromFile("ListPokemon.json")
            }
        )

        val response = service.getPokemon()
        val body = response.body()

        /**
         * Check if response is success and not null
         */
        Truth.assertThat(response.isSuccessful).isTrue()
        Truth.assertThat(body).isNotNull()
        /**
         * Check
         * - list size
         * - first object
         */
        val result = body!!.results
        Truth.assertThat(result.size).isEqualTo(1)
        Truth.assertThat(result.first().name).isEqualTo("bulbasaur")
        Truth.assertThat(result.first().url).isEqualTo("https://pokeapi.co/api/v2/pokemon/1/")
    }

    @Test
    fun `Get Pokemon Detail`() = runTest {
        mockWebServer.enqueue(
            MockResponse().apply {
                addHeader("Content-Type", "application/json; charset=utf-8")
                addHeader("Cache-Control", "no-cache")
                setResponseCode(200)
                setBodyFromFile("PokemonDetail.json")
            }
        )

        val response = service.getPokemonDetail(25)

        Truth.assertThat(response.isSuccessful).isTrue()
        Truth.assertThat(response.body()).isNotNull()

        val pokemon = response.body()!!
        Truth.assertThat(pokemon.id).isEqualTo(25)
        Truth.assertThat(pokemon.name).isEqualTo("pikachu")
        Truth.assertThat(pokemon.weight).isEqualTo(60)
        Truth.assertThat(pokemon.height).isEqualTo(4)

        //check abilities
        Truth.assertThat(pokemon.abilities).isNotNull()
        val abilities = pokemon.abilities!!
        Truth.assertThat(abilities).hasSize(1)
        Truth.assertThat(abilities.first().ability).isNotNull()
        Truth.assertThat(abilities.first().ability!!.name).isEqualTo("static")
        Truth.assertThat(abilities.first().ability!!.url)
            .isEqualTo("https://pokeapi.co/api/v2/ability/9/")
        Truth.assertThat(abilities.first().isHidden).isFalse()
        Truth.assertThat(abilities.first().slot).isEqualTo(1)

        //check moves
        Truth.assertThat(pokemon.moves).isNotNull()
        val moves = pokemon.moves!!
        Truth.assertThat(moves).hasSize(1)
        Truth.assertThat(moves.first().move).isNotNull()
        Truth.assertThat(moves.first().move!!.name).isEqualTo("mega-punch")
        Truth.assertThat(moves.first().move!!.url).isEqualTo("https://pokeapi.co/api/v2/move/5/")

        //check sprites
        Truth.assertThat(pokemon.sprites).isNotNull()
        val sprites = pokemon.sprites!!
        Truth.assertThat(sprites.backDefault)
            .isEqualTo("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/25.png")
        Truth.assertThat(sprites.backFemale)
            .isEqualTo("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/female/25.png")
        Truth.assertThat(sprites.backShiny)
            .isEqualTo("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/25.png")
        Truth.assertThat(sprites.backShinyFemale)
            .isEqualTo("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/female/25.png")
        Truth.assertThat(sprites.frontDefault)
            .isEqualTo("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/25.png")
        Truth.assertThat(sprites.frontFemale)
            .isEqualTo("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/female/25.png")
        Truth.assertThat(sprites.frontShiny)
            .isEqualTo("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/25.png")
        Truth.assertThat(sprites.frontShinyFemale)
            .isEqualTo("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/female/25.png")

    }

    @Test
    fun `Get Type`() = runTest {
        mockWebServer.enqueue(
            MockResponse().apply {
                addHeader("Content-Type", "application/json; charset=utf-8")
                addHeader("Cache-Control", "no-cache")
                setResponseCode(200)
                setBodyFromFile("Type.json")
            }
        )

        val response = service.getType()
        Truth.assertThat(response.isSuccessful).isTrue()
        Truth.assertThat(response.body()).isNotNull()

        val result = response.body()!!.results

        //check size
        Truth.assertThat(result).hasSize(20)

        //check sample on 1st element
        Truth.assertThat(result.first().name).isEqualTo("normal")
        Truth.assertThat(result.first().url).isEqualTo("https://pokeapi.co/api/v2/type/1/")
    }

    @Test
    fun `Get Damage Class`() = runTest {
        mockWebServer.enqueue(
            MockResponse().apply {
                addHeader("Content-Type", "application/json; charset=utf-8")
                addHeader("Cache-Control", "no-cache")
                setResponseCode(200)
                setBodyFromFile("DamageClass.json")
            }
        )

        val response = service.getDamageClass()
        Truth.assertThat(response.isSuccessful).isTrue()
        Truth.assertThat(response.body()?.results).isNotNull()

        val result = response.body()!!.results
        Truth.assertThat(result).hasSize(3)

        Truth.assertThat(result.first().name).isEqualTo("status")
        Truth.assertThat(result.first().url).isEqualTo("https://pokeapi.co/api/v2/move-damage-class/1/")
    }

    @Test
    fun `Get Move`() = runTest {
        mockWebServer.enqueue(
            MockResponse().apply {
                addHeader("Content-Type", "application/json; charset=utf-8")
                addHeader("Cache-Control", "no-cache")
                setResponseCode(200)
                setBodyFromFile("Move.json")
            }
        )

        val response = service.getMoves()
        Truth.assertThat(response.isSuccessful).isTrue()
        Truth.assertThat(response.body()).isNotNull()

        val result = response.body()!!.results

        //check size
        Truth.assertThat(result).hasSize(1)

        //check sampe on 1st element
        Truth.assertThat(result.first().name).isEqualTo("pound")
        Truth.assertThat(result.first().url).isEqualTo("https://pokeapi.co/api/v2/move/1/")
    }

    @Test
    fun `Get Move Detail`() = runTest {
        mockWebServer.enqueue(
            MockResponse().apply {
                addHeader("Content-Type", "application/json; charset=utf-8")
                addHeader("Cache-Control", "no-cache")
                setResponseCode(200)
                setBodyFromFile("MoveDetail.json")
            }
        )

        val response = service.getMove(1)
        Truth.assertThat(response.isSuccessful).isTrue()
        Truth.assertThat(response.body()).isNotNull()

        val result = response.body()!!
        Truth.assertThat(result.id).isEqualTo(85)
        Truth.assertThat(result.power).isEqualTo(90)
        Truth.assertThat(result.accuracy).isEqualTo(100)
        Truth.assertThat(result.pp).isEqualTo(15)

        Truth.assertThat(result.type).isNotNull()
        Truth.assertThat(result.type!!.name).isEqualTo("electric")
        Truth.assertThat(result.type!!.url).isEqualTo("https://pokeapi.co/api/v2/type/13/")

        Truth.assertThat(result.damageClass).isNotNull()
        Truth.assertThat(result.damageClass!!.name).isEqualTo("special")
        Truth.assertThat(result.damageClass!!.url).isEqualTo("https://pokeapi.co/api/v2/move-damage-class/3/")
    }

    @Test
    fun `Get Abilities`() = runTest {
        mockWebServer.enqueue(
            MockResponse().apply {
                addHeader("Content-Type", "application/json; charset=utf-8")
                addHeader("Cache-Control", "no-cache")
                setResponseCode(200)
                setBodyFromFile("ListAbilities.json")
            }
        )

        val response = service.getAbilities()
        Truth.assertThat(response.isSuccessful).isTrue()
        Truth.assertThat(response.body()).isNotNull()

        val result = response.body()!!.results

        Truth.assertThat(result).hasSize(1)

        //check sampe on 1st element
        Truth.assertThat(result.first().name).isEqualTo("stench")
        Truth.assertThat(result.first().url).isEqualTo("https://pokeapi.co/api/v2/ability/1/")
    }

    @Test
    fun `Get Ability Detail`() = runTest {
        mockWebServer.enqueue(
            MockResponse().apply {
                addHeader("Content-Type", "application/json; charset=utf-8")
                addHeader("Cache-Control", "no-cache")
                setResponseCode(200)
                setBodyFromFile("AbilityDetail.json")
            }
        )

        val response = service.getAbility(1)
        Truth.assertThat(response.isSuccessful).isTrue()
        Truth.assertThat(response.body()).isNotNull()

        val result = response.body()!!
        Truth.assertThat(result.id).isEqualTo(1)
        Truth.assertThat(result.name).isEqualTo("stench")

        Truth.assertThat(result.effectEntries).hasSize(1)
        Truth.assertThat(result.effectEntries!!.first().effect).isEqualTo("This Pokémon's damaging moves have a 10% chance to make the target flinch with each hit if they do not already cause flinching as a secondary effect.\n" +
                "\n" +
                "This ability does not stack with a held item.\n" +
                "\n" +
                "Overworld: The wild encounter rate is halved while this Pokémon is first in the party.")
        Truth.assertThat(result.effectEntries!!.first().shortEffect).isEqualTo("Has a 10% chance of making target Pokémon flinch with each hit.")
        Truth.assertThat(result.effectEntries!!.first().language).isNotNull()
        Truth.assertThat(result.effectEntries!!.first().language!!.name).isEqualTo("en")
        Truth.assertThat(result.effectEntries!!.first().language!!.url).isEqualTo("https://pokeapi.co/api/v2/language/9/")
    }
}