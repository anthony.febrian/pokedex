package com.rariki.pokedex.domain.usecase

import com.google.common.truth.Truth
import com.rariki.pokedex.domain.entity.ListPokemon
import com.rariki.pokedex.domain.usecase.interactor.SearchPokemonUseCaseImpl
import org.junit.Test

class SearchPokemonUseCaseTest {

    private val useCase: SearchPokemonUseCase = SearchPokemonUseCaseImpl()

    private val bulbasaur = ListPokemon(
        id = 1,
        name = "Bulbasaur",
        sprite = "b"
    )
    private val charmander = ListPokemon(
        id = 1,
        name = "Charmander",
        sprite = "C"
    )
    private val squirtle = ListPokemon(
        id = 1,
        name = "Squirtle",
        sprite = "S"
    )

    private val pokemon = listOf(
        bulbasaur,
        charmander,
        squirtle,
    )

    @Test
    fun `Search With Empty Keyword`() {
        val result = useCase("", pokemon)
        Truth.assertThat(result).hasSize(3)
    }

    @Test
    fun `Search Bulbasaur`() {
        val result = useCase("bulbasaur", pokemon)
        Truth.assertThat(result).hasSize(1)
        Truth.assertThat(result.first()).isEqualTo(bulbasaur)
    }

}