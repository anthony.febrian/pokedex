package com.rariki.pokedex.utils.extention

import okhttp3.mockwebserver.MockResponse
import okio.buffer
import okio.source
import java.nio.charset.StandardCharsets

internal fun MockResponse.setBodyFromFile(filename: String) {
    val inputStream = javaClass.classLoader?.getResourceAsStream(filename)
    val source = inputStream?.source()?.buffer()
    val body = source?.readString(StandardCharsets.UTF_8) ?: ""
    setBody(body = body)
}