package com.rariki.pokedex.data.local.dao

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth
import com.rariki.pokedex.data.local.AppDataBase
import com.rariki.pokedex.data.local.entity.move.MoveLocal
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class MoveDaoTest {

    private lateinit var db:AppDataBase
    private lateinit var dao:MoveDao

    @Before
    fun setup() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, AppDataBase::class.java)
            .allowMainThreadQueries()
            .build()
        dao = db.moveDao()
    }

    @After
    fun tearDown() {
        db.close()
    }

    @Test
    fun Insert_Move_Then_Check_On_Databse() = runTest {
        val move = MoveLocal(
            id = 1,
            name = "thunder",
            power = 2,
            accuracy = 100,
            pp = 10,
            typeId = 1,
            damageClassId = null,
        )
        dao.insert(move)

        val moves = dao.getMoves()
        Truth.assertThat(moves).hasSize(1)
    }

    @Test
    fun Insert_Duplicate_ID() = runTest {
        val move = MoveLocal(
            id = 1,
            name = "thunder",
            power = 2,
            accuracy = 100,
            pp = 10,
            typeId = 1,
            damageClassId = null,
        )
        val move2 = move.copy(
            power = 3,
            accuracy = 4,
            pp = 5,
            typeId = 6,
            damageClassId = 7,
        )

        dao.insert(move, move2)

        val moves = dao.getMoves()
        Truth.assertThat(moves).hasSize(1)

        val testMove = moves.first()
        Truth.assertThat(testMove.id).isEqualTo(1)
        Truth.assertThat(testMove.name).isEqualTo("thunder")
        Truth.assertThat(testMove.power).isEqualTo(3)
        Truth.assertThat(testMove.accuracy).isEqualTo(4)
        Truth.assertThat(testMove.pp).isEqualTo(5)
        Truth.assertThat(testMove.typeId).isEqualTo(6)
        Truth.assertThat(testMove.damageClassId).isEqualTo(7)
    }

    @Test
    fun Delete_Move() = runTest {
        val move = MoveLocal(
            id = 1,
            name = "thunder",
            power = 2,
            accuracy = 100,
            pp = 10,
            typeId = 1,
            damageClassId = null,
        )
        dao.insert(
            move,
            move.copy(id = 2),
            move.copy(id = 3)
        )

        val moves = dao.getMoves()
        Truth.assertThat(moves).hasSize(3)

        dao.deleteAll()
        val latestMoves = dao.getMoves()
        Truth.assertThat(latestMoves).hasSize(0)
    }
}