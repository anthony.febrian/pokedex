@file:OptIn(ExperimentalCoroutinesApi::class)

package com.rariki.pokedex.data.local.dao

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth
import com.rariki.pokedex.data.local.AppDataBase
import com.rariki.pokedex.data.local.entity.type.TypeLocal
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class TypeDaoTest {
    private lateinit var db: AppDataBase
    private lateinit var dao:TypeDao

    @Before
    fun setup() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, AppDataBase::class.java)
            .allowMainThreadQueries()
            .build()
        dao = db.typeDao()
    }

    @After
    fun tearDown() {
        db.close()
    }

    @Test
    fun Insert_Then_Check_On_Databse() = runTest {
        val type = TypeLocal(
            id = 1,
            name = "fire"
        )
        dao.insert(type)

        val types = dao.getTypes()
        Truth.assertThat(types).hasSize(1)
        Truth.assertThat(types.first()).isEqualTo(type)
    }

    @Test
    fun Insert_Duplicate_ID() = runTest {
        val type = TypeLocal(
            id = 1,
            name = "fire"
        )
        val type2 = type.copy(name = "water")

        dao.insert(type, type2)

        val types = dao.getTypes()
        Truth.assertThat(types).hasSize(1)
        Truth.assertThat(types.first()).isEqualTo(type2)
    }

    @Test
    fun Delete_Type() = runTest {
        val type = TypeLocal(
            id = 1,
            name = "fire"
        )
        dao.insert(type)

        val types = dao.getTypes()
        Truth.assertThat(types).hasSize(1)

        dao.deleteAll()
        val typesAfterDelete = dao.getTypes()
        Truth.assertThat(typesAfterDelete).hasSize(0)
    }
}