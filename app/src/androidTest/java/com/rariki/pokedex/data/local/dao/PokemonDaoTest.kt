@file:OptIn(ExperimentalCoroutinesApi::class)

package com.rariki.pokedex.data.local.dao

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth
import com.rariki.pokedex.data.local.AppDataBase
import com.rariki.pokedex.data.local.entity.ability.AbilityLocal
import com.rariki.pokedex.data.local.entity.move.MoveLocal
import com.rariki.pokedex.data.local.entity.pokemon.PokemonAbilityCrossRef
import com.rariki.pokedex.data.local.entity.pokemon.PokemonLocal
import com.rariki.pokedex.data.local.entity.pokemon.PokemonMoveCrossRef
import com.rariki.pokedex.data.local.entity.pokemon.PokemonTypeCrossRef
import com.rariki.pokedex.data.local.entity.type.TypeLocal
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class PokemonDaoTest {
    private lateinit var db: AppDataBase
    private lateinit var moveDao: MoveDao
    private lateinit var pokemonMoveDao: PokemonMoveDao
    private lateinit var pokemonDao: PokemonDao
    private lateinit var typeDao: TypeDao
    private lateinit var pokemonTypeDao: PokemonTypeDao
    private lateinit var abilityDao: AbilityDao
    private lateinit var pokemonAbilityDao: PokemonAbilityDao

    @Before
    fun setup() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, AppDataBase::class.java)
            .allowMainThreadQueries()
            .build()
        moveDao = db.moveDao()
        pokemonMoveDao = db.pokemonMoveDao()

        typeDao = db.typeDao()
        pokemonTypeDao = db.pokemonTypeDao()

        abilityDao = db.abilityDao()
        pokemonAbilityDao = db.pokemonAbilityDao()

        pokemonDao = db.pokemonDao()
    }

    @After
    fun tearDown() {
        db.close()
    }

    @Test
    fun Insert_Pokemon_Then_Get_Pokemon() = runTest {
        val pokemon = PokemonLocal(
            id = 1,
            name = "bulbasaur",
            weight = 2,
            height = 3,
            baseExp = 4,
            sprite = "sprite",
            spriteFemale = "sprite_female",
            officialArtWork = "official_art_work"
        )

        pokemonDao.insertPokemon(pokemon)
        val listPokemon = pokemonDao.getListPokemon()
        Truth.assertThat(listPokemon).hasSize(1)
    }

    @Test
    fun Insert_Pokemon_Then_Search_Pokemon() = runTest {
        //make sure on db still 0
        val initListPokemonFromDb = pokemonDao.getListPokemon()
        Truth.assertThat(initListPokemonFromDb).hasSize(0)

        val pokemon = PokemonLocal(
            id = 1,
            name = "bulbasaur",
            weight = 2,
            height = 3,
            baseExp = 4,
            sprite = "sprite",
            spriteFemale = "sprite_female",
            officialArtWork = "official_art_work"
        )
        pokemonDao.insertPokemon(pokemon)

        //test search pokemon
        val listPokemon = pokemonDao.searchPokemon("bulba")
        Truth.assertThat(listPokemon).hasSize(1)

        //test not available pokemon
        val listPokemon2 = pokemonDao.searchPokemon("agumon")
        Truth.assertThat(listPokemon2).hasSize(0)
    }

    @Test
    fun Insert_Pokemon_Then_Get_Pokemon_By_id() = runTest {
        //make sure on db still 0
        val initListPokemonFromDb = pokemonDao.getListPokemon()
        Truth.assertThat(initListPokemonFromDb).hasSize(0)

        val pokemon = PokemonLocal(
            id = 1,
            name = "bulbasaur",
            weight = 2,
            height = 3,
            baseExp = 4,
            sprite = "sprite",
            spriteFemale = "sprite_female",
            officialArtWork = "official_art_work"
        )
        pokemonDao.insertPokemon(pokemon)

        //get pokemon
        val pokemonDetails = pokemonDao.getPokemon(1)
        Truth.assertThat(pokemonDetails).isNotNull()

        Truth.assertThat(pokemonDetails!!.pokemon.id).isEqualTo(1)
        Truth.assertThat(pokemonDetails.pokemon.name).isEqualTo("bulbasaur")
        Truth.assertThat(pokemonDetails.pokemon.weight).isEqualTo(2)
        Truth.assertThat(pokemonDetails.pokemon.height).isEqualTo(3)
        Truth.assertThat(pokemonDetails.pokemon.baseExp).isEqualTo(4)
        Truth.assertThat(pokemonDetails.pokemon.sprite).isEqualTo("sprite")
        Truth.assertThat(pokemonDetails.pokemon.spriteFemale).isEqualTo("sprite_female")
        Truth.assertThat(pokemonDetails.pokemon.officialArtWork).isEqualTo("official_art_work")

        //test not available pokemon
        val listPokemon2 = pokemonDao.getPokemon(2)
        Truth.assertThat(listPokemon2).isNull()
    }

    @Test
    fun Test_Full_Detail_Pokemon() = runTest {
        //make sure on db still 0
        val initListPokemonFromDb = pokemonDao.getListPokemon()
        Truth.assertThat(initListPokemonFromDb).hasSize(0)

        //insert data
        val pokemon = PokemonLocal(
            id = 1,
            name = "bulbasaur",
            weight = 2,
            height = 3,
            baseExp = 4,
            sprite = "sprite",
            spriteFemale = "sprite_female",
            officialArtWork = "official_art_work"
        )
        pokemonDao.insertPokemon(pokemon)

        //insert move and cross ref
        val move = MoveLocal(
            id = 1,
            name = "razor leaf",
            power = 2,
            accuracy = 100,
            pp = 10,
            typeId = 1,
            damageClassId = null,
        )
        moveDao.insert(move)

        val pokemonMove = PokemonMoveCrossRef(
            pokemonId = pokemon.id,
            moveId = move.id
        )
        pokemonMoveDao.insert(pokemonMove)

        //insert type and cross ref
        val type = TypeLocal(
            id = 1,
            name = "grass"
        )
        typeDao.insert(type)
        val pokemonType = PokemonTypeCrossRef(
            pokemonId = pokemon.id,
            typeId = type.id
        )
        pokemonTypeDao.insert(pokemonType)

        //insert ability and cross ref
        val ability = AbilityLocal(
            id = 1,
            name = "static",
            description = "description"
        )
        abilityDao.insert(ability)
        val pokemonAbility = PokemonAbilityCrossRef(
            pokemonId = pokemon.id,
            abilityId = ability.id
        )
        pokemonAbilityDao.insert(pokemonAbility)

        //get pokemon
        val pokemonDetails = pokemonDao.getPokemon(1)
        Truth.assertThat(pokemonDetails).isNotNull()

        //check pokemon
        Truth.assertThat(pokemonDetails!!.pokemon.id).isEqualTo(1)
        Truth.assertThat(pokemonDetails.pokemon).isEqualTo(pokemon)

        //check move
        Truth.assertThat(pokemonDetails.moves).hasSize(1)
        Truth.assertThat(pokemonDetails.moves.first()).isEqualTo(move)

        //check type
        Truth.assertThat(pokemonDetails.types).hasSize(1)
        Truth.assertThat(pokemonDetails.types.first()).isEqualTo(type)

        //check ability
        Truth.assertThat(pokemonDetails.abilities).hasSize(1)
        Truth.assertThat(pokemonDetails.abilities.first()).isEqualTo(ability)
    }
}