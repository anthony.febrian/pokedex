@file:OptIn(ExperimentalCoroutinesApi::class)

package com.rariki.pokedex.data.local.dao

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth
import com.rariki.pokedex.data.local.AppDataBase
import com.rariki.pokedex.data.local.entity.ability.AbilityLocal
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AbilityDaoTest {

    private lateinit var db: AppDataBase
    private lateinit var dao: AbilityDao

    @Before
    fun setup() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, AppDataBase::class.java)
            .allowMainThreadQueries()
            .build()
        dao = db.abilityDao()
    }

    @After
    fun tearDown() {
        db.close()
    }

    @Test
    fun Insert_Then_Check_On_Databse() = runTest {
        val ability = AbilityLocal(
            id = 1,
            name = "static",
            description = "description"
        )
        dao.insert(ability)

        val types = dao.getAll()
        Truth.assertThat(types).hasSize(1)
        Truth.assertThat(types.first()).isEqualTo(ability)
    }

    @Test
    fun Insert_Duplicate_ID() = runTest {
        val ability = AbilityLocal(
            id = 1,
            name = "static",
            description = "description"
        )
        val ability2 = ability.copy(description = "this is static")

        dao.insert(ability, ability2)

        val types = dao.getAll()
        Truth.assertThat(types).hasSize(1)
        Truth.assertThat(types.first()).isEqualTo(ability2)
    }

    @Test
    fun Delete_Type() = runTest {
        val ability = AbilityLocal(
            id = 1,
            name = "static",
            description = "description"
        )
        dao.insert(ability)

        val types = dao.getAll()
        Truth.assertThat(types).hasSize(1)

        dao.deleteAll()
        val abiltiesAfterDelete = dao.getAll()
        Truth.assertThat(abiltiesAfterDelete).hasSize(0)
    }
}